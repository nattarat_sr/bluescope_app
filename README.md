# React base components

Learning project for creating component in pattern same as Semantic UI React.

You can run this project in local server by
```
npm start
```

You can build this project to production by
```
npm run build
```

You can run styleguide in local server to see components by
```
npx styleguidist server
```

and you can build a HTML version by
```
npx styleguidist build
```
