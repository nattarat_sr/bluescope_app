export const ROUTE_PATH = {
  HOME: {
    TEXT: 'Home',
    LINK: '/'
  },

  ABOUT: {
    TEXT: 'About',
    LINK: '/about'
  },

  TEST: {
    TEXT: 'Test',
    LINK: '/test'
  }
}
