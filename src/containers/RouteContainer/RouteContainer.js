import React from 'react'
import {
  Route,
  Switch
} from 'react-router-dom'
import {
  connect,
} from 'react-redux'
import {
  TransitionGroup,
  CSSTransition
} from 'react-transition-group'
import {
  ToastContainer,
  cssTransition
} from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import {
  setRouteHistory,
} from './../../actions'
import {
  MainLayout,
  HomePage,
  AboutPage,
  TestPage
} from './../../pages'
import {
  ROUTE_PATH,
} from './../../helpers'

export class RouteApp extends React.Component {

  constructor(props) {
    super(props)
    props.setRouteHistory(props.history)
    this.state = {}
  }

  render() {
    const {
      location
    } = this.props

    const toastAnimation = cssTransition({
      enter: 'slideDownFadeIn',
      exit: 'slideDownFadeOut',
      duration: 300
    })

    return (
      <React.Fragment>
        <MainLayout>
          <TransitionGroup className='transition-group'>
            <CSSTransition classNames='page'
              key={location.key}
              timeout={{
                // Consistent with CSS transition duration in TransitionContainer style
                // File path: src/components/TransitionContainer/styled.js
                // 0.5s = 500
                enter: 500,
                exit: 500
              }}
            >
              <Switch location={location}>
                {/* Use props 'exact' for match single container(not share container) */}
                <Route exact path={ROUTE_PATH.HOME.LINK} component={HomePage} />
                <Route exact path={ROUTE_PATH.ABOUT.LINK} component={AboutPage} />
                <Route exact path={ROUTE_PATH.TEST.LINK} component={TestPage} />
              </Switch>
            </CSSTransition>
          </TransitionGroup>
        </MainLayout>
        {/* React Toastify - global options */}
        <ToastContainer
          className='react-toastify'
          position='top-center'
          hideProgressBar
          closeOnClick={false}
          transition={toastAnimation}
        />
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => {
  return {

  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setRouteHistory: (data) => dispatch(setRouteHistory(data))
  }
}

export const RouteContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(RouteApp)