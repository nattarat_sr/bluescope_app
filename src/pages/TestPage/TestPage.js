import React from 'react'
import {
  css
} from 'styled-components'
import {
  toast
} from 'react-toastify'
import {
  ButtonBase,
  CmsLayoutBase,
  ContainerBase,
  FieldBase,
  GridBase,
  ImageBase,
  ModalBase,
  NotificationBase,
  SectionBase,
  ShapeContainerBase,
  TableBase,
  TextBase,
  ChartBase
} from './../../components/base'
import {
  Button,
  CmsLayout,
  Container,
  Field,
  Grid,
  Image,
  Modal,
  Notification,
  Section,
  ShapeContainer,
  Table,
  Text,
  Chart
} from './../../components'
import {
  TYPOGRAPHYS,
  VARIABLES,
  CONTENTS,
  ICONS,
  UTILITIES
} from './../../themes'

export class TestPage extends React.Component {
  // constructor() {
  //   super()
  //   this.state = {
  //   }
  // }

  // functions = {
  //   onClickXxxxx: () => {
  //     this.setState({
  //     })
  //   }
  // }

  // componentDidMount() {
  // }

  render() {
    // const {
    // } = this.state

    return (
      <React.Fragment>
        <h1>Test</h1>
      </React.Fragment>
    )
  }
}
