import React from 'react'
import {
  TransitionContainer,
  Container
} from './../../components'
// import {
//   UserListContainer,
// } from './../../containers'
import {
  ROUTE_PATH,
  redirect,
} from './../../helpers'

export class HomePage extends React.Component {
  render() {
    return (
      <TransitionContainer
        // motion='overlap-from'
      >
        <Container
          justify='center'
          align='center'
          height='100'
          heightUnit='vh'
          bgColor='lightgray'
        >
          {/* <UserListContainer /> */}
          <h1>{ROUTE_PATH.HOME.TEXT}</h1>
          <button
            onClick={() => {
              redirect(ROUTE_PATH.ABOUT.LINK)
            }}
          >
            Go to {ROUTE_PATH.ABOUT.TEXT}
          </button>
        </Container>
      </TransitionContainer>
    )
  }
}
