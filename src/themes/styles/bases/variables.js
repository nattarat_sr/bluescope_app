// //////////////////////////////////////////////////////////////////////////////////////////////////
// ==================================================================================================
//
// Variables:
//
// * Factors
// * Website width
// * Colors
// * Font
// * Z indexs
// * Breakpoints
// * Transitions
// * Animation timings
// * Components
//   - Scrollbar
//
// ==================================================================================================
// //////////////////////////////////////////////////////////////////////////////////////////////////

// Factors (using in variables)
// ============================================================
const SPACING_FACTOR = 5
const GRID_SYSTEM = 12

export default {
  // Factors
  // ============================================================
  FACTORS: {
    SPACING: SPACING_FACTOR,
    GRID: GRID_SYSTEM
  },

  // Website width
  // ============================================================
  WEBSITE_WIDTH: 1440,

  // Colors
  // ============================================================
  COLORS: {
    // Base
    // ------------------------------
    // Transparent
    TRANSPARENT: 'transparent',
    // Black
    BLACK: '#000000',
    // White
    WHITE: '#FFFFFF',
    // Gray
    // GRAY_1: '',
    // Red
    // RED_1: '',
    // Green
    // GREEN_1: '',
    // Blue
    // BLUE_1: '',
    // Overlay
    // OVERLAY_1: '',
    // Shadow
    // SHADOW_1: '',

    // Apply - bring 'Base' color create a new name for easy using
    // ------------------------------
    // Primary
    PRIMARY_1: '#000000',
    // Status
    DISABLED: '#EEEEEE',
    ERROR: '#FF6565',
    SUCCESS: '#51A380',
    // Scrollbar
    SCROLLBAR_DEFAULT: '#B5B5B5',
    SCROLLBAR_HOVER: '#858585',
    SCROLLBAR_ACTIVE: '#858585'
  },

  // Font
  // ============================================================
  FONT: {
    FAMILIES: {
      FIRST: {
        WEIGHTS: {
          // REGULAR: 'Xxxxx-Regular',
        },
        SIZES: {
          // FS_XX: XX,
        },
        // LINE_HEIGHT_FACTOR: X.X
      }
    }
  },

  // Zindexs
  // ============================================================
  Z_INDEXS: {
    LV_1:  1,
    LV_2:  9,
    LV_3:  10,
    LV_4:  11,
    LV_5:  99,
    LV_6:  100,
    LV_7:  101,
    LV_8:  999,
    LV_9:  1000,
    LV_10: 1001
  },

  // Breakpoints
  // ============================================================
  BREAKPOINTS: {
    // Mobile
    BP_320: '320px',
    BP_321: '321px',
    BP_359: '359px',
    BP_360: '360px',
    BP_361: '361px',
    BP_374: '374px',
    BP_375: '375px',
    BP_376: '376px',
    BP_413: '413px',
    BP_414: '414px',
    BP_415: '415px',

    // Phablet
    BP_479: '479px',
    BP_480: '480px',
    BP_481: '481px',
    BP_639: '639px',
    BP_640: '640px',
    BP_641: '641px',
    BP_666: '666px',
    BP_667: '667px',
    BP_668: '668px',
    BP_735: '735px',
    BP_736: '736px',
    BP_737: '737px',

    // Tablet
    BP_767: '767px',
    BP_768: '768px',
    BP_769: '769px',
    BP_811: '811px',
    BP_812: '812px',
    BP_813: '813px',
    BP_1023: '1023px',
    BP_1024: '1024px',
    BP_1025: '1025px',

    // Laptop
    BP_1279: '1279px',
    BP_1280: '1280px',
    BP_1281: '1281px',
    BP_1365: '1365px',
    BP_1366: '1366px',
    BP_1367: '1367px',
    BP_1439: '1439px',
    BP_1440: '1440px',
    BP_1441: '1441px',
    BP_1599: '1599px',
    BP_1600: '1600px',
    BP_1601: '1601px',

    // Desktop
    BP_1919: '1919px',
    BP_1920: '1920px',
    BP_1921: '1921px',
    BP_2559: '2559px',
    BP_2560: '2560px'
  },

  // Transitions
  // ============================================================
  TRANSITIONS: {
    DEFAULT: 'all 0.3s ease'
  },

  // Animation timings
  // ============================================================
  ANIMATION_TIMINGS: {
    ELASTIC: 'cubic-bezier(.835, -.005, .06, 1)'
  },

  // Components
  // ============================================================

  // Button
  // ------------------------------
  BUTTON: {
    HEIGHTS: {
      H_50: 50
    },
    BORDER_RADIUSES: {
      BR_4: 4
    }
  },

  // Scrollbar
  // ------------------------------
  SCROLLBAR: {
    WIDTH: 8,
    HEIGHT: 8,
    BORDER_RADIUS: 8
  },

  // Field
  // ------------------------------
  FIELD: {
    // WIDTHS: {
    //   W_440: 440
    // },
    // HEIGHTS: {
    //   H_44: 44
    // },
    // BORDER: {
    //   WIDTHS: {
    //     W_2: 2
    //   },
    //   RADIUSES: {
    //     RD_6: 6
    //   }
    // },
    // CHECKBOX: {
    //   SIZE: 18,
    //   BORDER: {
    //     WIDTH: 2,
    //     RADIUS: 6
    //   }
    // }
  },

  // Modal
  // ------------------------------
  MODAL: {
    WIDTHS: {
      W_320: 320
    },

    CONTAINER: {
      C_1: {
        TRANSFORM_START: 'translateY(-30px)',
        TRANSFORM_END: 'translateY(0)',
        SPACING: 30,
        BUTTON_CLOSE_SPACING_TOP: 15,
        BUTTON_CLOSE_SPACING_RIGHT: 15
      }
    },

    HEADER: {
      H_1: {
        PADDING: 15,
        BORDER_BOTTOM_WIDTH: 1,
        BORDER_BOTTOM_STYLE: 'solid',
        BORDER_TOP_RIGHT_RADIUS: 10,
        BORDER_TOP_LEFT_RADIUS: 10
      }
    },

    BODY: {
      B_1: {
        PADDING: 15
      }
    },

    FOOTER: {
      F_1: {
        PADDING: 15,
        BORDER_TOP_WIDTH: 1,
        BORDER_TOP_STYLE: 'solid',
        BORDER_BOTTOM_RIGHT_RADIUS: 10,
        BORDER_BOTTOM_LEFT_RADIUS: 10
      }
    }
  },

  // CMS Layout
  // ------------------------------
  CMS_LAYOUT: {
    TOPBAR: {
      HEIGHT: 70
    },

    SIDEBAR: {
      WIDTH: 260
    },

    CONTENT: {
    }
  },

  // Notification
  // ------------------------------
  NOTIFICATION: {
    WIDTHS: {
      DEFAULT: 665 // Not use 'W_' becuase this variable is global
    },
    BUTTON_CLOSE: {
      WIDTH: 75
    }
  }
}
