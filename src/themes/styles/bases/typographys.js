import {
  css
} from 'styled-components'
import {
  default as VARIABLES
} from './../../../themes/styles/bases/variables' // Use relative path for React Styleguidist

// //////////////////////////////////////////////////////////////////////////////////////////////////
// ==================================================================================================
//
// Typographys:
//
// First (Roboto)
// ------------------------------
// * Regular
//   - TYPOGRAPHYS.FIRST.REGULAR.FS_XX
//
// Notice: Color eg. plain text, link etc. can set at variables.js or 'color' props of Text/TextBase component
//
// ==================================================================================================
// //////////////////////////////////////////////////////////////////////////////////////////////////

// Text default style
// ============================================================
// const FONT_STYLES = {
//   DEFAULT: css`
//     font-weight: normal;
//     text-transform: none;
//   `,
//   FAMILIES: {
//     FIRST: {
//       REGULAR: css`
//         font-family: ${VARIABLES.FONT.FAMILIES.FIRST.WEIGHTS.REGULAR};
//       `
//     }
//   },
//   SIZES: {
//     FIRST: {
//       FS_XX: css`
//         font-size: ${VARIABLES.FONT.FAMILIES.FIRST.SIZES.FS_XX + `px`};
//         line-height: ${Math.round(VARIABLES.FONT.FAMILIES.FIRST.SIZES.FS_XX * VARIABLES.FONT.FAMILIES.FIRST.LINE_HEIGHT_FACTOR) + `px`};
//       `
//     }
//   }
// }

export default {
  // First (XXXXX)
  // ============================================================
  // FIRST: {
  //   // Regular
  //   // ------------------------------
  //   REGULAR: {
  //     // Font size > XXpx
  //     FS_XX: css`
  //       ${FONT_STYLES.DEFAULT}
  //       ${FONT_STYLES.FAMILIES.FIRST.REGULAR}
  //       ${FONT_STYLES.SIZES.FIRST.FS_XX}
  //     `
  //   },
  // }
}
