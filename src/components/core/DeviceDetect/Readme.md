Default:

```jsx
<DeviceDetect>
  Device environment classes:
  <br />
  * .is-android
  <br />
  * .is-ios
  <br />
  * .is-chrome
  <br />
  * .is-safari
  <br />
  * .is-mobile-safari
</DeviceDetect>
```
