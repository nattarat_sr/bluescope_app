import React from 'react'
import ClassNames from 'classnames'
// import PropTypes from 'prop-types'
import {
  TemplateBase
} from './../../base'
// import {
// } from './../../../components'

// -------------------- Please remove this comment after reading message. --------------------
// Notice: Cause of import separation is styguidist require a root path for generate document.
// -------------------------------------------------------------------------------------------

// import {
//   default as VARIABLES
// } from './../../../themes/styles/bases/variables'
// import {
//   default as TYPOGRAPHYS
// } from './../../../themes/styles/bases/typographys'
// import {
//   default as UTILITIES
// } from './../../../themes/styles/helpers/utilities'

/**
 * TemplateBase description:
 * - TemplateBase
 */

export class TemplateForBase extends React.PureComponent {
  render () {
    const {
      className,
      modifierName
    } = this.props

    // props for css classes
    const classes = ClassNames(
      className,
      {'is-modifier-name': modifierName}
    )

    return (
      <TemplateBase
        {...this.props}
        className={classes}
      >
        {/* ============================================================
          PLEASE READ BEFORE BEGIN COMPONENT CREATION !!!
          This is template for base component wrapper.
          if you haven't base component, you should use TemplateForCustom for component creation.
        ================================================================ */}
      </TemplateBase>
    )
  }

  static defaultProps = {
    // * If you set new default props, you should create propsType for correct styleguide document.
  }

  static propTypes = {
    // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * props_name_oneType
    */
    // props_name_onetype: PropTypes.string,

    /**
    * props_name_oneMoreType
    */
    // props_name_oneMoreType: PropTypes.oneOfType([
    //   PropTypes.node,
    //   PropTypes.string
    // ])
  }
}
