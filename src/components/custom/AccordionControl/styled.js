import {
  injectGlobal
} from 'styled-components'
import {
  default as VARIABLES
} from './../../../themes/styles/bases/variables'
// import {
//   default as TYPOGRAPHYS
// } from './../../../themes/styles/bases/typographys'
// import {
//   default as MIXINS
// } from './../../../themes/styles/helpers/mixins'

// Sample using image in style
// url(${require('./../../../themes/images/icons/icon-sample.svg')});

injectGlobal`
  .accordion-control-base {
    /* Animation
    ------------------------------- */
    @keyframes fade-in {
      0% {
          opacity: 0;
      }

      100% {
          opacity: 1;
      }
    }

    /* Parent
    ------------------------------- */
    /* Childrens
    ------------------------------- */
    /* Container */
    .accordion {
    }

    /* List */
    .accordion__item {

      &:last-child {}
    }

    /* Button */
    .accordion__title {

      &:focus {}

      /* Arrow */
      &:before,
      &:after {}

      &:before {}

      &:after {}

      /* Open content */
      &[aria-selected='true'] {
        &:before {}

        &:after {}
      }
    }

    /* Content */
    .accordion__body {
      &.accordion__body--hidden {}
    }

    /* Modifiers
    ------------------------------- */
    /* Dropdown */
    &.is-dropdown {

      /* Container */
      .accordion {}

      /* List */
      .accordion__item {}

      /* Button */
      .accordion__title {}

      /* Content */
      .accordion__body {
        transition: ${VARIABLES.TRANSITIONS.DEFAULT};
        transform: translateY(0);
        opacity: 1;

        &.accordion__body--hidden {
          transform: translateY(15px);
          opacity: 0;
        }
      }
    }

    /* Media queries
    ------------------------------- */
  }
`
