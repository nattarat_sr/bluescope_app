import React from 'react'
import ClassNames from 'classnames'
// import PropTypes from 'prop-types'
import {
  CmsLayoutBase
} from './../../base'
// import {
// } from './../../../components'
// import {
//   default as VARIABLES
// } from './../../../themes/styles/bases/variables'
// import {
//   default as TYPOGRAPHYS
// } from './../../../themes/styles/bases/typographys'
// import {
//   default as UTILITIES
// } from './../../../themes/styles/helpers/utilities'

/**
 * CmsLayout description:
 * - CmsLayout
 */

class CmsLayoutTopbar extends React.PureComponent {
  render () {
    return (
      <CmsLayoutBase.Topbar
        {...this.props}
      />
    )
  }

  static defaultProps = {
    // * If you set new default props, you should create propsType for correct styleguide document.
  }
}

class CmsLayoutSidebar extends React.PureComponent {
  render () {
    return (
      <CmsLayoutBase.Sidebar
        {...this.props}
      />
    )
  }

  static defaultProps = {
    // * If you set new default props, you should create propsType for correct styleguide document.
  }
}

class CmsLayoutContent extends React.PureComponent {
  render () {
    return (
      <CmsLayoutBase.Content
        {...this.props}
      />
    )
  }

  static defaultProps = {
    // * If you set new default props, you should create propsType for correct styleguide document.
  }
}

export class CmsLayout extends React.PureComponent {
  render () {
    const {
      className
    } = this.props

    // props for css classes
    const classes = ClassNames(
      className,
      // { 'is-modifier-name': modifierName }
    )

    return (
      <CmsLayoutBase
        {...this.props}
        className={classes}
      />
    )
  }

  static Topbar = CmsLayoutTopbar
  static Sidebar = CmsLayoutSidebar
  static Content = CmsLayoutContent

  static defaultProps = {
    // * If you set new default props, you should create propsType for correct styleguide document.
  }

  static propTypes = {
    // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * props_name_oneType
    */
    // props_name_onetype: PropTypes.string,

    /**
    * props_name_oneMoreType
    */
    // props_name_oneMoreType: PropTypes.oneOfType([
    //   PropTypes.node,
    //   PropTypes.string
    // ])
  }
}
