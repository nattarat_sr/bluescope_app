Pie:

```jsx
<ChartBase
  pie
  innerRadius={50}
  outerRadius={100}
  fill='#333333'
  data={[
    { name: 'Group A', value: 2400 },
    { name: 'Group B', value: 4567 },
    { name: 'Group C', value: 1398 },
    { name: 'Group D', value: 9800 },
    { name: 'Group E', value: 3908 },
    { name: 'Group F', value: 4800 }
  ]}
/>
```

Pie with label:

```jsx
<ChartBase
  pie
  label
  width={300}
  height={256}
  cx={146}
  cy={127}
  innerRadius={50}
  outerRadius={100}
  fill='#333333'
  data={[
    { name: 'Group A', value: 2400 },
    { name: 'Group B', value: 4567 },
    { name: 'Group C', value: 1398 },
    { name: 'Group D', value: 9800 },
    { name: 'Group E', value: 3908 },
    { name: 'Group F', value: 4800 }
  ]}
/>
<div className='partition-line' />
<div className='notice'>Note: if you use 'label' props, you must set 'width', 'height', 'cx', 'cy' props for control chart size and position</div>
```

Progress:

```jsx
<ChartBase
  progress
  progressWidth='300'
  progressWidthUnit='px'
  progressHeight='8'
  progressTrackPadding='2'
  progressTrackColor='#000000'
  progressColor='#FFFFFF'
  progressPercent='50'
/>
```

Progress with percent number:

```jsx
<ChartBase
  progress
  progressWidth='300'
  progressWidthUnit='px'
  progressHeight='8'
  progressTrackPadding='2'
  progressTrackColor='#000000'
  progressColor='#FFFFFF'
  progressPercent='50'
  // percentNumberFontStyle={
  //   css`
  //     font-family: Tahoma;
  //     font-weight: normal;
  //     font-size: 12px;
  //     text-transform: none;
  //   `
  // }
  percentNumberSpacingLeft='10'
/>
<div className='notice'>Note: if you use 'percentNumberFontStyle' props, you must import css from styled-components</div>
```
