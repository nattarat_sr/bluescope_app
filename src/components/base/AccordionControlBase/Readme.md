Default:

```jsx
<div className='notice'>Note: AccordionControl is a wrapper for control Accordion component from <a href='https://github.com/springload/react-accessible-accordion' target='_blank'>https://github.com/springload/react-accessible-accordion</a>. you can overwrite style or create class for control style via AccordionControl. Default structure can see at <a href='https://gist.github.com/Nattarat/c8329ff1ca82f12a111964d7e341fe19#file-react-accessible-accordion-js' target='_blank'>https://gist.github.com/Nattarat/c8329ff1ca82f12a111964d7e341fe19#file-react-accessible-accordion-js</a></div>
```
