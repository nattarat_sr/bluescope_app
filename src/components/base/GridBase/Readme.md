Default:

```jsx
<GridBase>
  <GridBase.Column>
    <div className='box is-light'>Col_1</div>
  </GridBase.Column>
  <GridBase.Column>
    <div className='box is-dark'>Col_2</div>
  </GridBase.Column>
  <GridBase.Column>
    <div className='box is-light'>Col_3</div>
  </GridBase.Column>
</GridBase>
```

Flex:

```jsx
<GridBase>
  <GridBase.Column flex='1'>
    <div className='box is-light'>Col_1</div>
  </GridBase.Column>
  <GridBase.Column flex='1'>
    <div className='box is-dark'>Col_2</div>
  </GridBase.Column>
  <GridBase.Column flex='1'>
    <div className='box is-light'>Col_3</div>
  </GridBase.Column>
</GridBase>
```

Flex responsive:

```jsx
<GridBase>
  <GridBase.Column flexMobile='3' flexPhablet='1' flexTablet='3' flexLaptop='1' flexDesktop='3' flexLargeDesktop='1'>
    <div className='box is-light'>Col_1</div>
  </GridBase.Column>
  <GridBase.Column flexMobile='2' flexPhablet='2' flexTablet='2' flexLaptop='2' flexDesktop='2' flexLargeDesktop='2'>
    <div className='box is-dark'>Col_2</div>
  </GridBase.Column>
  <GridBase.Column flexMobile='1' flexPhablet='3' flexTablet='1' flexLaptop='3' flexDesktop='1' flexLargeDesktop='3'>
    <div className='box is-light'>Col_3</div>
  </GridBase.Column>
</GridBase>
```

Flex Stack:

```jsx
<GridBase>
  <GridBase.Column flex='1' flexStackMobile>
    <div className='box is-light'>Stack Mobile</div>
  </GridBase.Column>
  <GridBase.Column flex='1'>
    <div className='box is-dark'>Col</div>
  </GridBase.Column>
  <GridBase.Column flex='1'>
    <div className='box is-light'>Col</div>
  </GridBase.Column>
</GridBase>
<div className='partition-line' />
<GridBase>
  <GridBase.Column flex='1' flexStackMobile flexStackPhablet>
    <div className='box is-light'>Stack Mobile to Phablet</div>
  </GridBase.Column>
  <GridBase.Column flex='1'>
    <div className='box is-dark'>Col</div>
  </GridBase.Column>
  <GridBase.Column flex='1'>
    <div className='box is-light'>Col</div>
  </GridBase.Column>
</GridBase>
<div className='partition-line' />
<GridBase>
  <GridBase.Column flex='1' flexStackMobile flexStackPhablet flexStackTablet>
    <div className='box is-light'>Stack Mobile to Tablet</div>
  </GridBase.Column>
  <GridBase.Column flex='1'>
    <div className='box is-dark'>Col</div>
  </GridBase.Column>
  <GridBase.Column flex='1'>
    <div className='box is-light'>Col</div>
  </GridBase.Column>
</GridBase>
```

Grid:

```jsx
<GridBase>
  <GridBase.Column grid='4'>
    <div className='box is-light'>Col_1</div>
  </GridBase.Column>
  <GridBase.Column grid='4'>
    <div className='box is-dark'>Col_2</div>
  </GridBase.Column>
  <GridBase.Column grid='4'>
    <div className='box is-light'>Col_3</div>
  </GridBase.Column>
</GridBase>
```

Grid responsive:

```jsx
<GridBase>
  <GridBase.Column gridMobile='12' gridPhablet='4' gridTablet='12' gridLaptop='4' gridDesktop='12' gridLargeDesktop='4'>
    <div className='box is-light'>Col_1</div>
  </GridBase.Column>
  <GridBase.Column gridMobile='12' gridPhablet='4' gridTablet='12' gridLaptop='4' gridDesktop='12' gridLargeDesktop='4'>
    <div className='box is-dark'>Col_2</div>
  </GridBase.Column>
  <GridBase.Column gridMobile='12' gridPhablet='4' gridTablet='12' gridLaptop='4' gridDesktop='12' gridLargeDesktop='4'>
    <div className='box is-light'>Col_3</div>
  </GridBase.Column>
</GridBase>
```

Grid gutter horizontal/vetical:

```jsx
<GridBase gutter='15' gutterVertical='15'>
  <GridBase.Column grid='4'>
    <div className='box is-light'>Col_1</div>
  </GridBase.Column>
  <GridBase.Column grid='4'>
    <div className='box is-dark'>Col_2</div>
  </GridBase.Column>
  <GridBase.Column grid='4'>
    <div className='box is-light'>Col_3</div>
  </GridBase.Column>
  <GridBase.Column grid='4'>
    <div className='box is-light'>Col_4</div>
  </GridBase.Column>
  <GridBase.Column grid='4'>
    <div className='box is-dark'>Col_5</div>
  </GridBase.Column>
  <GridBase.Column grid='4'>
    <div className='box is-light'>Col_6</div>
  </GridBase.Column>
</GridBase>
```

Grid gutter horizontal/vetical responsive:

```jsx
<GridBase
  gutterMobile='15'
  gutterVerticalMobile='15'
  gutterPhablet='30'
  gutterVerticalPhablet='30'
  gutterTablet='15'
  gutterVerticalTablet='15'
  gutterLaptop='30'
  gutterVerticalLaptop='30'
  gutterDesktop='15'
  gutterVerticalDesktop='15'
  gutterLargeDesktop='30'
  gutterVerticalLargeDesktop='30'
>
  <GridBase.Column grid='4'>
    <div className='box is-light'>Col_1</div>
  </GridBase.Column>
  <GridBase.Column grid='4'>
    <div className='box is-dark'>Col_2</div>
  </GridBase.Column>
  <GridBase.Column grid='4'>
    <div className='box is-light'>Col_3</div>
  </GridBase.Column>
  <GridBase.Column grid='4'>
    <div className='box is-light'>Col_4</div>
  </GridBase.Column>
  <GridBase.Column grid='4'>
    <div className='box is-dark'>Col_5</div>
  </GridBase.Column>
  <GridBase.Column grid='4'>
    <div className='box is-light'>Col_6</div>
  </GridBase.Column>
</GridBase>
```

Grid gutter unit:

```jsx
<GridBase gutterUnit='em' gutter='5' gutterVertical='5'>
  <GridBase.Column grid='4'>
    <div className='box is-light'>Col_1</div>
  </GridBase.Column>
  <GridBase.Column grid='4'>
    <div className='box is-dark'>Col_2</div>
  </GridBase.Column>
  <GridBase.Column grid='4'>
    <div className='box is-light'>Col_3</div>
  </GridBase.Column>
  <GridBase.Column grid='4'>
    <div className='box is-light'>Col_4</div>
  </GridBase.Column>
  <GridBase.Column grid='4'>
    <div className='box is-dark'>Col_5</div>
  </GridBase.Column>
  <GridBase.Column grid='4'>
    <div className='box is-light'>Col_6</div>
  </GridBase.Column>
</GridBase>
```

Justify:

```jsx
<GridBase justify='flex-end'>
  <GridBase.Column>
    <div className='box is-light'>Col_1</div>
  </GridBase.Column>
  <GridBase.Column>
    <div className='box is-dark'>Col_2</div>
  </GridBase.Column>
  <GridBase.Column>
    <div className='box is-light'>Col_3</div>
  </GridBase.Column>
</GridBase>
```

Justify responsive:

```jsx
<GridBase
  justifyMobile='flex-end'
  justifyPhablet='center'
  justifyTablet='flex-end'
  justifyLaptop='center'
  justifyDesktop='flex-end'
  justifyLargeDesktop='center'
>
  <GridBase.Column>
    <div className='box is-light'>Col_1</div>
  </GridBase.Column>
  <GridBase.Column>
    <div className='box is-dark'>Col_2</div>
  </GridBase.Column>
  <GridBase.Column>
    <div className='box is-light'>Col_3</div>
  </GridBase.Column>
</GridBase>
```