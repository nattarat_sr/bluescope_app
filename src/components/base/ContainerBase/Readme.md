Default:

```jsx
<ContainerBase>Default</ContainerBase>
```

Width/Height/BackgroundColor:

```jsx
<ContainerBase width='300' height='100' bgColor='#CCCCCC'>
  <div>Width/Height/BackgroundColor</div>
</ContainerBase>
```

Width/Height unit:

```jsx
<ContainerBase width='30' widthUnit='vw' height='5' heightUnit='vw' bgColor='#CCCCCC'>
  <div>Width/Height/BackgroundColor</div>
</ContainerBase>
<div className='partition-line' />
<div className='heading'>Responsive</div>
<ContainerBase width='30' widthUnit='vw' widthMobile='300' widthMobileUnit='px' height='5' heightUnit='vw' heightMobile='100' heightMobileUnit='px' bgColor='#CCCCCC'>
  <div>Width/Height/BackgroundColor</div>
</ContainerBase>
```

Calculate height:

```jsx
<ContainerBase
  // calcHeight='100vh - 400px'
  calcMinHeight='100vh - 400px'
  bgColor='#CCCCCC'
>
  <div>Calculate height</div>
</ContainerBase>
```

Padding:

```jsx
<ContainerBase padding='30' bgColor='#CCCCCC'>
  <div>Padding all</div>
</ContainerBase>
<div className='partition-line' />
<ContainerBase paddingTop='30' paddingRight='30' paddingBottom='30' paddingLeft='30' bgColor='#CCCCCC'>
  <div>Padding (separate side props)</div>
</ContainerBase>
<div className='partition-line' />
<div className='heading'>Responsive</div>
<ContainerBase paddingMobile='30' bgColor='#CCCCCC'>
  <div>Padding all at  mobile screen size (width less than 414px)</div>
</ContainerBase>
<div className='partition-line' />
<ContainerBase paddingTopMobile='30' paddingRightMobile='30' paddingBottomMobile='30' paddingLeftMobile='30' bgColor='#CCCCCC'>
  <div>Padding (separate side props) at  mobile screen size (width less than 414px)</div>
</ContainerBase>
<div className='partition-line' />
<div className='notice'>Note: Responsive props has 'Mobile'(width less than 414px), 'Phablet'(width 414px - 767px) and 'Tablet'(width 768px to 1279px). You can change breakpoint by 'breakpointPaddingMobile', 'breakpointPaddingPhabletMin', 'breakpointPaddingPhabletMax',  'breakpointPaddingTabletMin' and 'breakpointPaddingTabletMax' props</div>
```

Horizontal align:

```jsx
<div className='heading'>Left</div>
<ContainerBase bgColor='#CCCCCC'>
  <div className='box is-dark'>Element</div>
</ContainerBase>
<div className='partition-line' />
<div className='heading'>Center</div>
<ContainerBase align='center' bgColor='#CCCCCC'>
  <div className='box is-dark'>Element</div>
</ContainerBase>
<div className='partition-line' />
<div className='heading'>Right</div>
<ContainerBase align='flex-end' bgColor='#CCCCCC'>
  <div className='box is-dark'>Element</div>
</ContainerBase>
```

Vertical align:

```jsx
<div className='heading'>Top</div>
<ContainerBase height='100' bgColor='#CCCCCC'>
  <div className='box is-dark'>Element</div>
</ContainerBase>
<div className='partition-line' />
<div className='heading'>Middle</div>
<ContainerBase height='100' justify='center' bgColor='#CCCCCC'>
  <div className='box is-dark'>Element</div>
</ContainerBase>
<div className='partition-line' />
<div className='heading'>Bottom</div>
<ContainerBase height='100' justify='flex-end' bgColor='#CCCCCC'>
  <div className='box is-dark'>Element</div>
</ContainerBase>
```

Background image/size/repeat/position:

```jsx
<ContainerBase
  height='450'
  bgImage={require('./images/grid-number.jpg')}
  bgSize='cover'
  bgRepeat='no-repeat'
  // bgPositionX='center'
  // bgPositionY='center'
  bgPosition='center'
>
  Background
</ContainerBase>
```
