Default:

```jsx
<div className='notice'>Note: Can't display modal in styleguide because modal style is position 'fixed' cause to hide all viewport</div>
<div className='partition-line' />
/* Please Copy code > paste to your react project and change open props to 'true' state for open modal
<ModalBase
  open={false}
  // enableOverlayClose={false}
  width='320' // VARIABLES.MODAL.WIDTHS.W_320
  overlayColor='rgba(0, 0, 0, 0.5)'
  spacing='30' // VARIABLES.MODAL.CONTAINER.C_1.SPACING
  // spacingTop='30'
  // spacingRight='30'
  // spacingBottom='30'
  // spacingLeft='30'
  // spacingHorizontal='30'
  // spacingVertical='30'
  buttonClose={
    <div>Close</div>
  }
  buttonCloseSpacingTop='15' // VARIABLES.MODAL.CONTAINER.C_1.BUTTON_CLOSE_SPACING_TOP
  buttonCloseSpacingRight='15'  // VARIABLES.MODAL.CONTAINER.C_1.BUTTON_CLOSE_SPACING_RIGHT
  transition='all 0.3s ease'
  transformStart='translateY(-30px)' // VARIABLES.MODAL.CONTAINER.C_1.TRANSFORM_START
  transformEnd='translateY(0)' // VARIABLES.MODAL.CONTAINER.C_1.TRANSFORM_END
  onClickClose={() => {}}
>
  <ModalBase.Section
    padding='15' // VARIABLES.MODAL.HEADER.H_1.PADDING
    // paddingTop='15'
    // paddingRight='15'
    // paddingBottom='15'
    // paddingLeft='15'
    // paddingHorizontal='15'
    // paddingVertical='15'
    // bgColor='#FFFFFF'
    // borderTopWidth='1'
    // borderTopStyle='solid'
    // borderTopColor='#CCCCCC'
    borderBottomWidth='1' // VARIABLES.MODAL.HEADER.H_1.BORDER_BOTTOM_WIDTH
    borderBottomStyle='solid' // VARIABLES.MODAL.HEADER.H_1.BORDER_BOTTOM_STYLE
    borderBottomColor='#CCCCCC'
    borderTopRightRadius='10' // VARIABLES.MODAL.HEADER.H_1.BORDER_TOP_RIGHT_RADIUS
    borderTopLeftRadius='10' // VARIABLES.MODAL.HEADER.H_1.BORDER_TOP_LEFT_RADIUS
    // borderBottomRightRadius='10'
    // borderBottomLeftRadius='10'
    // borderRadius='10'
  >
    Hedaer
  </ModalBase.Section>
  <ModalBase.Section
    padding='15' // VARIABLES.MODAL.BODY.B_1.PADDING
    // paddingTop='15'
    // paddingRight='15'
    // paddingBottom='15'
    // paddingLeft='15'
    // paddingHorizontal='15'
    // paddingVertical='15'
    // bgColor='#FFFFFF'
    // borderTopWidth='1'
    // borderTopStyle='solid'
    // borderTopColor='#CCCCCC'
    // borderBottomWidth='1'
    // borderBottomStyle='solid'
    // borderBottomColor='#CCCCCC'
    // borderTopRightRadius='10'
    // borderTopLeftRadius='10'
    // borderBottomRightRadius='10'
    // borderBottomLeftRadius='10'
    // borderRadius='10'
  >
    Body
  </ModalBase.Section>
  <ModalBase.Section
    padding='15' //VARIABLES.MODAL.FOOTER.F_1.PADDING
    // paddingTop='15'
    // paddingRight='15'
    // paddingBottom='15'
    // paddingLeft='15'
    // paddingHorizontal='15'
    // paddingVertical='15'
    // bgColor='#FFFFFF'
    borderTopWidth='1' //VARIABLES.MODAL.FOOTER.F_1.BORDER_TOP_WIDTH
    borderTopStyle='solid' //VARIABLES.MODAL.FOOTER.F_1.BORDER_TOP_STYLE
    borderTopColor='#CCCCCC'
    // borderBottomWidth='1'
    // borderBottomStyle='solid'
    // borderBottomColor='#CCCCCC'
    // borderTopRightRadius='10'
    // borderTopLeftRadius='10'
    borderBottomRightRadius='10' //VARIABLES.MODAL.FOOTER.F_1.BORDER_BOTTOM_RIGHT_RADIUS
    borderBottomLeftRadius='10' //VARIABLES.MODAL.FOOTER.F_1.BORDER_BOTTOM_LEFT_RADIUS
    // borderRadius='10'
  >
    Footer
  </ModalBase.Section>
</ModalBase>
*/
```
