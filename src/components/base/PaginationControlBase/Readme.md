Default:

```jsx
<div className='notice'>Note: PaginationControl is a wrapper for control Pagination component from <a href='https://github.com/vayser/react-js-pagination' target='_blank'>https://github.com/vayser/react-js-pagination</a>. you can overwrite style or create class for control style via AccordionControl. Default structure can see at <a href='https://gist.github.com/Nattarat/c8329ff1ca82f12a111964d7e341fe19#file-react-js-pagination-js' target='_blank'>https://gist.github.com/Nattarat/c8329ff1ca82f12a111964d7e341fe19#file-react-js-pagination-js</a></div>
```
