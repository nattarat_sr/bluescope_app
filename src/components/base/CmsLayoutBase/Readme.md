Default:

```jsx
<CmsLayoutBase
  bgColor='#FFFFFF'
>
  <CmsLayoutBase.Topbar
    // topbarHeight={}
    // topbarPadding={}
    // topbarPaddingTop={}
    // topbarPaddingRight={}
    // topbarPaddingBottom={}
    // topbarPaddingLeft={}
    topbarPaddingHorizontal='30'
    // topbarPaddingVertical={}
    topbarBgColor='#AAAAAA'
    // topbarBorderBottomWidth={}
    // topbarBorderBottomStyle={}
    // topbarBorderBottomColor={}
    // topbarBoxShadow={}
  >
    <div>Logo</div>
    <div>Account menu</div>
  </CmsLayoutBase.Topbar>
  <CmsLayoutBase.Sidebar
    sidebarWidth='250'
    // sidebarPadding={}
    // sidebarPaddingTop={}
    // sidebarPaddingRight={}
    // sidebarPaddingBottom={}
    // sidebarPaddingLeft={}
    sidebarPaddingHorizontal='30'
    sidebarPaddingVertical='30'
    sidebarBgColor='#DDDDDD'
    // sidebarBoxShadow={}
  >
    Sidebar
  </CmsLayoutBase.Sidebar>
  <CmsLayoutBase.Content
    // contentPadding={}
    contentPaddingTop='30'
    contentPaddingRight='30'
    contentPaddingBottom='30'
    contentPaddingLeft='30'
    // contentPaddingHorizontal={}
    // contentPaddingVertical={}
    contentBgColor='#EEEEEE'
  >
    Content
  </CmsLayoutBase.Content>
</CmsLayoutBase>
```
