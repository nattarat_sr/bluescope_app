Default:

```jsx
<ButtonBase>
  <span className='text is-white'>Button</span>
</ButtonBase>
```

Width/Height/Color:

```jsx
<ButtonBase width='300' height='50' bgColor='#1978C8'>
  <span className='text is-white'>Button</span>
</ButtonBase>
```

Padding:

```jsx
<div className='heading'>All</div>
<ButtonBase padding='15'>
  <span className='text is-white'>Button</span>
</ButtonBase>
<div className='partition-line' />
<div className='heading'>Horizontal/Vertical</div>
<ButtonBase paddingHorizontal='15' paddingVertical='15'>
  <span className='text is-white'>Button</span>
</ButtonBase>
<div className='partition-line' />
<div className='heading'>Separate</div>
<ButtonBase paddingTop='15' paddingBottom='15' paddingRight='15' paddingLeft='15'>
  <span className='text is-white'>Button</span>
</ButtonBase>
```

BorderRadius:

```jsx
<ButtonBase width='100' height='30' borderRadius='30'>
  <span className='text is-white'>Button</span>
</ButtonBase>
```

Fluid:

```jsx
<ButtonBase fluid>
  <span className='text is-white'>Button</span>
</ButtonBase>
```

Disabled:

```jsx
<ButtonBase disabled>
  <span className='text is-white'>Button</span>
</ButtonBase>
<div className='partition-line' />
<div className='heading'>Change disabled color</div>
<ButtonBase disabled bgColorDisabled='#AAAAAA'>
  <span className='text is-white'>Button</span>
</ButtonBase>
```

onClick:

```jsx
<ButtonBase onClick={() => {alert('Clicked')}}>
  <span className='text is-white'>Button</span>
</ButtonBase>
```

Icon:

```jsx
<ButtonBase padding='15'>
  <ButtonBase.Icon spacingRight='10'>
    <span className='text is-white'>Front icon</span>
  </ButtonBase.Icon>
  <span className='text is-white'>Button</span>
  <ButtonBase.Icon spacingLeft='10'>
    <span className='text is-white'>Back icon</span>
  </ButtonBase.Icon>
</ButtonBase>
```
