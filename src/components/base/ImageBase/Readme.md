Default:

```jsx
<ImageBase src={require('./images/sample-image.jpg')} />
<div className='partition-line' />
<div className='notice'>Default image ratio is 16:9, width is a fluid, Ratio unit is a percent</div>
```

Ratio unit > Percent:

```jsx
<ImageBase
  heightRatio='75'
  src={require('./images/sample-image.jpg')}
/>
<div className='partition-line' />
<div className='notice'>Default width ratio is 100</div>
```

Ratio unit > Pixel:

```jsx
<ImageBase
  widthRatio='100'
  widthRatioUnit='px'
  heightRatio='100'
  heightRatioUnit='px'
  src={require('./images/sample-image.jpg')}
/>
```

Ratio unit > Percent & Pixel:

```jsx
<ImageBase
  widthRatio='50'
  heightRatio='100'
  heightRatioUnit='px'
  src={require('./images/sample-image.jpg')}
/>
```

Border radius:

```jsx
<ImageBase
  widthRatio='100'
  widthRatioUnit='px'
  heightRatio='100'
  heightRatioUnit='px'
  borderRadius='50'
  src={require('./images/sample-image.jpg')}
/>
```

Position Top/Left:

```jsx
<ImageBase
  top='10'
  left='10'
  src={require('./images/sample-image.jpg')}
  alt='Sample'
/>
<div className='partition-line' />
<div className='notice'>Use for control fine position</div>
```

Altenate text:

```jsx
<ImageBase
  src={require('./images/sample-image.jpg')}
  alt='Sample'
/>
```

OnClick:

```jsx
<ImageBase
  src={require('./images/sample-image.jpg')}
  onClick={() => {alert('Click!!!')}}
/>
```
