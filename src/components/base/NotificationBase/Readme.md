Default:

```jsx
<NotificationBase
  padding='15'
  // paddingTop='15'
  // paddingRight='15'
  // paddingBottom='15'
  // paddingLeft='15'
  // paddingHorizontal='15'
  // paddingVertical='15'
  spacing='15'
  // spacingTop='15'
  // spacingRight='15'
  // spacingBottom='15'
  // spacingLeft='15'
  // spacingHorizontal='15'
  // spacingVertical='15'
  bgColor='#C9F7CB'
  borderWidth='1'
  borderStyle='solid'
  borderColor='#51A380'
  borderRadius='6px'
  boxShadow='0 3px 5px rgba(0, 0, 0, 0.25)'
>
  Default
</NotificationBase>
<div className='partition-line' />
<div className='notice'>Note: You can use 'Grid' component for separate section in notification.</div>
```
