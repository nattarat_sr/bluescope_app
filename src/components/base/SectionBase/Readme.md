Default:

```jsx
<SectionBase>Default</SectionBase>
```

Width/Height/BackgroundColor:

```jsx
<SectionBase
  width='300'
  height='175'
  bgColor='#CCCCCC'
>
  Content
</SectionBase>
```

Calculate height:

```jsx
<SectionBase
  // calcHeight='100vh - 400px'
  calcMinHeight='100vh - 400px'
  bgColor='#CCCCCC'
>
  <div>Calculate height</div>
</SectionBase>
```

Width/Height unit:

```jsx
<SectionBase
  width='50'
  widthUnit='%'
  height='25'
  heightUnit='vh'
  bgColor='#CCCCCC'
>
  Content
</SectionBase>
```

Width/Height unit:

```jsx
<SectionBase
  scroll
  width='300'
  height='150'
  bgColor='#CCCCCC'
>
  <div style={{ height: '300px' }}>Scroll Content</div>
</SectionBase>
```

Flex alignment:

```jsx
<SectionBase
  width='300'
  height='300'
  bgColor='#CCCCCC'
  // flex='' // eg. 1, none
  // direction='' // eg. row, column
  justify='center'
  align='center'
>
  Content
</SectionBase>
<div className='partition-line' />
<SectionBase
  width='300'
  height='300'
  bgColor='#CCCCCC'
  // flex='' // eg. 1, none
  // direction='' // eg. row, column
  justify='space-between'
  align='flex-end'
>
  <div>Content_Left</div>
  <div>Content_Right</div>
</SectionBase>
```

Padding:

```jsx
<SectionBase
  padding='30'
  // paddingTop='30'
  // paddingRight='30'
  // paddingBottom='30'
  // paddingLeft='30'
  // paddingHorizontal='30'
  // paddingVertical='30'
  bgColor='#CCCCCC'
>
  Content
</SectionBase>
```

Spacing:

```jsx
<SectionBase
  // spacing='30'
  spacingTop='30'
  // spacingRight='30'
  // spacingBottom='30'
  spacingLeft='100'
  // spacingHorizontal='30'
  // spacingVertical='30'
  bgColor='#CCCCCC'
>
  Content
</SectionBase>
```

Border Top:

```jsx
<SectionBase
  borderTopWidth='2'
  borderTopStyle='solid'
  borderTopColor='#000000'
  // borderRightWidth='2'
  // borderRightStyle='solid'
  // borderRightColor='#000000'
  borderBottomWidth='2'
  borderBottomStyle='solid'
  borderBottomColor='#000000'
  // borderLeftWidth='2'
  // borderLeftStyle='solid'
  // borderLeftColor='#000000'
>
  Content
</SectionBase>
```
