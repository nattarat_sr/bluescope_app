Default:

```jsx
<FieldBase />
```

Textarea:

```jsx
<FieldBase textarea />
```

Fluid:

```jsx
<FieldBase fluid />
```

Error:

```jsx
<FieldBase
  error
  message='Error message'
/>
```

Success:

```jsx
<FieldBase
  success
  message='Success message'
/>
```

Disabled:

```jsx
<FieldBase disabled />
```

Field:

```jsx
<FieldBase
  width='200'
  height='36'
  placeholder='Placeholder...'
  placeholderColor='#AAAAAA'
  // paddingTop='10'
  // paddingRight='10'
  // paddingBottom='10'
  // paddingLeft='10'
  paddingHorizontal='10'
  // paddingVertical='10'
  // bgColor='#EEEEEE'
  borderWidth='2'
  borderStyle='solid'
  borderColor='#000000'
  borderRadius='6'
  // fontStyle={TYPOGRAPHYS.FIRST.REGULAR.FS_XX}
  fontColor='#000000'
  // textAlign='center'
  // inputTextType='password'
  id='sample'
  name='sample'
  // value='Sample'
  onChange={() => {}}
/>
```

Border bottom:

```jsx
<FieldBase
  borderBottomWidth='2'
  borderBottomStyle='solid'
  borderBottomColor='#000000'
  borderBottomRadius='6'
/>
```

Label:

```jsx
<FieldBase
  label='Label'
  // labelFontStyle={TYPOGRAPHYS.FIRST.REGULAR.FS_XX}
  labelFontColor='#0000FF'
  labelWidth='50'
  labelSpacingTop='1'
  labelSpacingRight='10'
/>
<div className='partition-line' />
<div className='notice'>Notice: comment props because styleguidist can't import TYPOGRAPHYS and VARIABLES from themes folder.</div>
```

Message:

```jsx
<FieldBase
  message='Message'
  // messageFontStyle={TYPOGRAPHYS.FIRST.REGULAR.FS_XX}
  messageFontColor='#0000FF'
  messageSpacingTop='5'
  messageSpacingLeft='10'
/>
<div className='partition-line' />
<div className='notice'>Notice: comment props because styleguidist can't import TYPOGRAPHYS and VARIABLES from themes folder.</div>
```

Hint:

```jsx
<FieldBase
  hint='Hint'
  // hintFontStyle={TYPOGRAPHYS.FIRST.REGULAR.FS_XX}
  hintFontColor='#0000FF'
  hintSpacingTop='5'
  hintSpacingLeft='10'
/>
<div className='partition-line' />
<div className='notice'>Notice: comment props because styleguidist can't import TYPOGRAPHYS and VARIABLES from themes folder.</div>
```

Icon in field:

```jsx
<FieldBase
  height='36'
  borderWidth='1'
  borderStyle='solid'
  borderColor='#000000'
  fieldIcon={<img src={require('./images/icon-search.png')} alt='Icon' />}
  // fieldIconWidth='36'
  // fieldIconSpacingLeft='5'
  fieldIconSpacingRight='10'
/>
```

Select:

```jsx
<FieldBase
  type='select'
  width='200'
  height='36'
  // paddingTop='10'
  paddingRight='36'
  // paddingBottom='10'
  paddingLeft='10'
  // paddingHorizontal='10'
  // paddingVertical='10'
  // bgColor='#EEEEEE'
  borderWidth='2'
  borderStyle='solid'
  borderColor='#000000'
  borderRadius='6'
  fontColor='#000000'
  id='sampleselect'
  name='sampleselect'
  fieldIcon={<img src={require('./images/icon-arrow.png')} alt='Icon' />}
  fieldIconWidth='36'
  fieldIconSpacingRight='2'
  onChange={() => {}}
>
  <option>Item_1</option>
  <option>Item_2</option>
  <option>Item_3</option>
</FieldBase>
```

Checkbox:

```jsx
<FieldBase
  checked
  type='checkbox'
  checkboxLabel='Label'
  checkboxSize='24'
  checkboxBgColor='#FFFFFF'
  checkboxCheckedBgColor='#51A380'
  checkboxBorderWidth='2'
  checkboxBorderStyle='solid'
  checkboxBorderColor='#000000'
  checkboxBorderCheckedColor='#51A380'
  checkboxBorderRadius='4'
  // checkboxLabelFontStyle={TYPOGRAPHYS.FIRST.REGULAR.FS_XX}
  checkboxLabelSpacingLeft='10'
  checkboxLabelFontColor='#000000'
  checkboxLabelCheckedFontColor='#0000FF'
  checkboxCheckedIcon={<img src={require('./images/icon-checked.png')} alt='Icon' style={{ width: '12px' }} />}
  onChange={() => {}}
/>
```

Radio:

```jsx
<FieldBase
  checked
  type='radio'
  radioLabel='Label'
  radioSize='24'
  radioBgColor='#FFFFFF'
  radioCheckedBgColor='#51A380'
  radioBorderWidth='2'
  radioBorderStyle='solid'
  radioBorderColor='#000000'
  radioBorderCheckedColor='#51A380'
  radioBorderRadius='12'
  // radioLabelFontStyle={TYPOGRAPHYS.FIRST.REGULAR.FS_XX}
  radioLabelSpacingLeft='10'
  radioLabelFontColor='#000000'
  radioLabelCheckedFontColor='#0000FF'
  radioCheckedIcon={<img src={require('./images/icon-checked.png')} alt='Icon' style={{ width: '12px' }} />}
  onChange={() => {}}
/>
```

File:

```jsx
<FieldBase
  type='file'
  fileName='sample-file-name.jpg'
  accept='.jpg, .png, .gif, .svg' // All file types > 'image/*, video/*, audio/*'
  buttonBrowse={<button>Browse</button>}
  // buttonBrowse={
  //   <ButtonBase width='100' height='30' borderRadius='30'>
  //     <TextBase
  //       fontStyle={TYPOGRAPHYS.FIRST.REGULAR.FS_XX}
  //       color={VARIABLES.COLORS.WHITE}
  //     >
  //       Browse
  //     </TextBase>
  //   </ButtonBase>
  // }
  buttonUpload={<button>Upload</button>}
  // buttonUpload={
  //   <ButtonBase width='100' height='30' borderRadius='30'>
  //     <TextBase
  //       fontStyle={TYPOGRAPHYS.FIRST.REGULAR.FS_XX}
  //       color={VARIABLES.COLORS.WHITE}
  //     >
  //       Upload
  //     </TextBase>
  //   </ButtonBase>
  // }
  buttonBrowseSpacingLeft='15'
  buttonUploadSpacingLeft='15'
  width='200'
  height='36'
  // paddingTop='10'
  // paddingRight='10'
  // paddingBottom='10'
  // paddingLeft='10'
  paddingHorizontal='10'
  // paddingVertical='10'
  // bgColor='#EEEEEE'
  borderWidth='2'
  borderStyle='solid'
  borderColor='#000000'
  borderRadius='6'
  // fontStyle={TYPOGRAPHYS.FIRST.REGULAR.FS_XX}
  fontColor='#000000'
  id='sample'
  name='sample'
  // value='Sample'
  onChange={() => {}}
/>
<div className='partition-line' />
<div className='notice'>Notice: comment props because styleguidist can't import TYPOGRAPHYS and VARIABLES from themes folder.</div>
```

File Avatar:

```jsx
<FieldBase
  type='fileAvatar'
  fileImage={
    <img src={require('./images/placeholder-user.png')} alt='Placeholder' />
  }
  accept='.jpg, .png, .gif, .svg' // All file types > 'image/*, video/*, audio/*'
  buttonBrowse={
    <button>Browse</button>
  }
  buttonDeleteUpload={
    <button>Upload</button>
  }
  // buttonBrowseSpacingLeft='15'
  buttonDeleteUploadSpacingLeft='15'
  id='sample'
  name='sample'
  // value='Sample'
  onChange={() => { }}
/>
```

Info:

```jsx
<FieldBase
  type='info'
  label='Label'
  labelWidth='50'
  info='Information'
  // fontStyle={TYPOGRAPHYS.FIRST.REGULAR.FS_XX}
/>
<div className='partition-line' />
<div className='notice'>Notice: comment props because styleguidist can't import TYPOGRAPHYS and VARIABLES from themes folder.</div>
```

Switch:

```jsx
<FieldBase
  // checked
  type='switch'
  switchWidth='175'
  switchHeight='36'
  switchBgColor='#DDDDDD'
  switchActiveBgColor='#00FF00'
  switchBorderWidth='2'
  switchBorderStyle='solid'
  switchBorderColor='#000000'
  switchBorderRadius='36'
  switchButtonSpacingLeft='5'
  switchButtonSpacingRight='170'
  switchButtonSize='26'
  switchButtonInactiveColor='#AAAAAA'
  switchButtonActiveColor='#FFFFFF'
  switchButtonShadow='0 1px 3px rgba(0, 0, 0, .25)'
  // switchNameFontStyle={TYPOGRAPHYS.FIRST.REGULAR.FS_XX}
  // switchNameOuterFontStyle={TYPOGRAPHYS.FIRST.REGULAR.FS_XX}
  switchNameOuterSpacingLeft='10'
  switchName='Switch name'
  switchOuterName='Switch outer name'
  onChange={() => {}}
/>
<div className='partition-line' />
<div className='notice'>Notice: comment props because styleguidist can't import TYPOGRAPHYS and VARIABLES from themes folder.</div>
```
