Default:

```jsx
<TextBase>Default</TextBase>
```

FontStyle/Width/Color/Ellipsis/SingleLine/VericalAlign/href/onClick:

```jsx
<TextBase
  // fontStyle={TYPOGRAPHYS.FIRST.REGULAR.FS_12}
  // width='100' // use with ellipsis
  // color={VARIABLES.COLORS.BLACK}
  // ellipsis
  // singleLine // line-height > 1
  // verticalAlign='middle' // default > baseline
  // href=''
  // onClick={() => {}}
>
  Notice: comment props because styleguidist can't import TYPOGRAPHYS and VARIABLES from themes folder.
</TextBase>
```

Font style responsive:

```jsx
<TextBase
  // fontStyleMobileSm={TYPOGRAPHYS.FIRST.REGULAR.FS_12}
  // fontStyleMobileMd={TYPOGRAPHYS.FIRST.REGULAR.FS_18}
  // fontStyleMobileLg={TYPOGRAPHYS.FIRST.REGULAR.FS_24}
  // fontStylePhabletSm={TYPOGRAPHYS.FIRST.REGULAR.FS_32}
  // fontStylePhabletMd={TYPOGRAPHYS.FIRST.REGULAR.FS_12}
  // fontStylePhabletLg={TYPOGRAPHYS.FIRST.REGULAR.FS_18}
  // fontStyleTabletSm={TYPOGRAPHYS.FIRST.REGULAR.FS_24}
  // fontStyleTabletMd={TYPOGRAPHYS.FIRST.REGULAR.FS_32}
  // fontStyleTabletLg={TYPOGRAPHYS.FIRST.REGULAR.FS_12}
>
  Notice: comment props because styleguidist can't import TYPOGRAPHYS and VARIABLES from themes folder.
</TextBase>
```
