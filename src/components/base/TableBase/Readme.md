Default:

```jsx
<TableBase>
  <TableBase.Section>
    <TableBase.Row>
      <TableBase.Column
        maxWidth='200'
        minWidth='200'
        // minHeight='50'
        padding='10'
        // paddingTop='10'
        // paddingRight='10'
        // paddingBottom='10'
        // paddingLeft='10'
        // paddingHorizontal='10'
        // paddingVertical='10'
        // bgColor='#EEEEEE'
        // borderWidth='1'
        // borderStyle='solid'
        // borderColor='#CCCCCC'
        borderBottomWidth='2'
        borderBottomStyle='solid'
        borderBottomColor='#CCCCCC'
        sortAscending={false}
        sortButtonAscending={
          <img
            style={{ width: '12px', height: '8px' }}
            src={require('./images/icon-arrow-ascending.svg')}
          />
        }
        sortButtonDescending={
          <img
            style={{ width: '12px', height: '8px' }}
            src={require('./images/icon-arrow-descending.svg')}
          />
        }
        onClickSort={() => {alert('Sort!!!')}}
      >
        Head_1
      </TableBase.Column>
      <TableBase.Column
        // maxWidth='200'
        minWidth='200' // For preserve width in small screen
        padding='10'
        // paddingTop='10'
        // paddingRight='10'
        // paddingBottom='10'
        // paddingLeft='10'
        // paddingHorizontal='10'
        // paddingVertical='10'
        // bgColor='#EEEEEE'
        // borderWidth='1'
        // borderStyle='solid'
        // borderColor='#CCCCCC'
        borderBottomWidth='2'
        borderBottomStyle='solid'
        borderBottomColor='#CCCCCC'
        alignCenter
      >
        Head_2
      </TableBase.Column>
      <TableBase.Column
        maxWidth='200'
        minWidth='200'
        padding='10'
        // paddingTop='10'
        // paddingRight='10'
        // paddingBottom='10'
        // paddingLeft='10'
        // paddingHorizontal='10'
        // paddingVertical='10'
        // bgColor='#EEEEEE'
        // borderWidth='1'
        // borderStyle='solid'
        // borderColor='#CCCCCC'
        borderBottomWidth='2'
        borderBottomStyle='solid'
        borderBottomColor='#CCCCCC'
        alignRight
      >
        Head_3
      </TableBase.Column>
    </TableBase.Row>
  </TableBase.Section>
  <TableBase.Section>
    <TableBase.Row>
      <TableBase.Column
        maxWidth='200'
        minWidth='200'
        padding='10'
        // paddingTop='10'
        // paddingRight='10'
        // paddingBottom='10'
        // paddingLeft='10'
        // paddingHorizontal='10'
        // paddingVertical='10'
        // bgColor='#EEEEEE'
        // borderWidth='1'
        // borderStyle='solid'
        // borderColor='#CCCCCC'
        // borderBottomWidth='1'
        // borderBottomStyle='solid'
        // borderBottomColor='#CCCCCC'
        borderTopLeftRadius='5'
        borderBottomLeftRadius='5'
      >
        Col_1 Lorem ipsum dolor, sit amet consectetur adipisicing elit. Placeat explicabo facilis repudiandae minus velit totam.
      </TableBase.Column>
      <TableBase.Column
        // maxWidth='200'
        minWidth='200'
        padding='10'
        // paddingTop='10'
        // paddingRight='10'
        // paddingBottom='10'
        // paddingLeft='10'
        // paddingHorizontal='10'
        // paddingVertical='10'
        // bgColor='#EEEEEE'
        // borderWidth='1'
        // borderStyle='solid'
        // borderColor='#CCCCCC'
        // borderBottomWidth='1'
        // borderBottomStyle='solid'
        // borderBottomColor='#CCCCCC'
        alignCenter
      >
        Col_2
      </TableBase.Column>
      <TableBase.Column
        maxWidth='200'
        minWidth='200'
        padding='10'
        // paddingTop='10'
        // paddingRight='10'
        // paddingBottom='10'
        // paddingLeft='10'
        // paddingHorizontal='10'
        // paddingVertical='10'
        // bgColor='#EEEEEE'
        // borderWidth='1'
        // borderStyle='solid'
        // borderColor='#CCCCCC'
        // borderBottomWidth='1'
        // borderBottomStyle='solid'
        // borderBottomColor='#CCCCCC'
        borderTopRightRadius='5'
        borderBottomRightRadius='5'
        alignRight
      >
        Col_3
      </TableBase.Column>
    </TableBase.Row>
    <TableBase.Row>
      <TableBase.Column
        maxWidth='200'
        minWidth='200'
        padding='10'
        // paddingTop='10'
        // paddingRight='10'
        // paddingBottom='10'
        // paddingLeft='10'
        // paddingHorizontal='10'
        // paddingVertical='10'
        bgColor='#EEEEEE'
        // borderWidth='1'
        // borderStyle='solid'
        // borderColor='#CCCCCC'
        // borderBottomWidth='1'
        // borderBottomStyle='solid'
        // borderBottomColor='#CCCCCC'
        borderTopLeftRadius='5'
        borderBottomLeftRadius='5'
      >
        Col_1
      </TableBase.Column>
      <TableBase.Column
        // maxWidth='200'
        minWidth='200'
        padding='10'
        // paddingTop='10'
        // paddingRight='10'
        // paddingBottom='10'
        // paddingLeft='10'
        // paddingHorizontal='10'
        // paddingVertical='10'
        bgColor='#EEEEEE'
        // borderWidth='1'
        // borderStyle='solid'
        // borderColor='#CCCCCC'
        // borderBottomWidth='1'
        // borderBottomStyle='solid'
        // borderBottomColor='#CCCCCC'
        alignCenter
      >
        Col_2
      </TableBase.Column>
      <TableBase.Column
        maxWidth='200'
        minWidth='200'
        padding='10'
        // paddingTop='10'
        // paddingRight='10'
        // paddingBottom='10'
        // paddingLeft='10'
        // paddingHorizontal='10'
        // paddingVertical='10'
        bgColor='#EEEEEE'
        // borderWidth='1'
        // borderStyle='solid'
        // borderColor='#CCCCCC'
        // borderBottomWidth='1'
        // borderBottomStyle='solid'
        // borderBottomColor='#CCCCCC'
        borderTopRightRadius='5'
        borderBottomRightRadius='5'
        alignRight
      >
        Col_3
      </TableBase.Column>
    </TableBase.Row>
  </TableBase.Section>
</TableBase>
```
