Default:

```jsx
<ShapeContainerBase
  // fluid
  // direction={}
  // wrap={}
  // justify={}
  // align={}
  // width={}
  // widthUnit={}
  // height={}
  // calcHeight={}
  // heightUnit={}
  // minHeight={}
  // padding={}
  // paddingTop={}
  // paddingRight={}
  // paddingBottom={}
  // paddingLeft={}
  // paddingHorizontal={}
  // paddingVertical={}
  // spacing={}
  // spacingTop={}
  // spacingRight={}
  // spacingBottom={}
  // spacingLeft={}
  // spacingHorizontal={}
  // spacingVertical={}
  // bgColor={}
  // bgHoverColor={}
  // borderWidth={}
  // borderStyle={}
  // borderColor={}
  // borderHoverColor={}
  // borderTopWidth={}
  // borderTopStyle={}
  // borderTopColor={}
  // borderTopHoverColor={}
  // borderRightWidth={}
  // borderRightStyle={}
  // borderRightColor={}
  // borderRightHoverColor={}
  // borderBottomWidth={}
  // borderBottomStyle={}
  // borderBottomColor={}
  // borderBottomHoverColor={}
  // borderLeftWidth={}
  // borderLeftStyle={}
  // borderLeftColor={}
  // borderLeftHoverColor={}
  // borderRadius={}
  // boxShadow={}
>
  Default
</ShapeContainerBase>
```

Calculate height:

```jsx
<ShapeContainerBase
  // calcHeight='100vh - 400px'
  calcMinHeight='100vh - 400px'
  bgColor='#CCCCCC'
>
  <div>Calculate height</div>
</ShapeContainerBase>
```

Panel:

```jsx
<ShapeContainerBase
  fluid
  height='300'
  padding='30'
  bgColor='#FFFFFF'
  borderRadius='10'
  boxShadow='0 1px 5px rgba(0, 0, 0, 0.15)'
>
  <div>Content_1</div>
  <div>Content_2</div>
</ShapeContainerBase>
```

Circle:

```jsx
<ShapeContainerBase
  justify='center'
  align='center'
  width='75'
  height='75'
  bgColor='#CCCCCC'
  borderWidth='3'
  borderStyle='solid'
  borderColor='#000000'
  borderRadius='50'
>
  Icon
</ShapeContainerBase>
```

Badge:

```jsx
<ShapeContainerBase
  justify='center'
  align='center'
  height='24'
  paddingHorizontal='15'
  bgColor='#FFCC00'
  borderRadius='24'
>
  Badge
</ShapeContainerBase>
```

Hover/onClick:

```jsx
<ShapeContainerBase
  justify='center'
  align='center'
  height='32'
  paddingHorizontal='15'
  bgColor='#00FF00'
  bgHoverColor='#FFCC00'
  borderWidth='3'
  borderStyle='solid'
  borderColor='#000000'
  borderHoverColor='#999999'
  borderRadius='16'
  onClick={() => {}}
>
  Badge
</ShapeContainerBase>
```
